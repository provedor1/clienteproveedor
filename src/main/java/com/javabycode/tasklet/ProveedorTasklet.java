package com.javabycode.tasklet;

import com.javabycode.model.Proveedor;
import com.javabycode.repository.ProveedorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.*;
import java.util.List;


@Setter
@Getter
public class ProveedorTasklet implements Tasklet {

    private String valorInicial;

    private ProveedorRepository proveedorRepository;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        String userDirectory = System.getProperty("user.home");
        userDirectory = userDirectory.replaceAll("\\\\","/");

        List<Proveedor> bbddProveedor = proveedorRepository.findAllByClienteId(valorInicial) ;

        String archcsvinterviniente = userDirectory + "/Downloads/File.txt";
        File  objFile = new File  (archcsvinterviniente);
        BufferedWriter pw = new BufferedWriter(new FileWriter(objFile));
        if(bbddProveedor.size()==0){
         System.out.println("el cliente no tiene proveedores asignados.");
        }else {
            for (Proveedor linea : bbddProveedor) {
                pw.write(linea.getId_proveedor() + ", " + linea.getNombre() + ", "
                        + linea.getFecha_de_alta() + ", " + linea.getId_cliente() + "\n");
            }
            pw.close();
        }
        System.exit(0);
        return RepeatStatus.FINISHED;
    }
}
