package com.javabycode.repository;

import com.javabycode.model.Proveedor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {

    @Query(value =
            "SELECT * " +
            "FROM VIEWNEXT.PROVEEDOR " +
            "WHERE ID_CLIENTE = ?1",
            nativeQuery = true)
    List<Proveedor> findAllByClienteId(String idCliente);


}
