package com.javabycode.batch;
 
import javax.sql.DataSource;

import com.javabycode.repository.ProveedorRepository;
import com.javabycode.tasklet.ProveedorTasklet;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@EnableBatchProcessing
public class BatchJobConfig {
     
    @Autowired
    private JobBuilderFactory jobBuilder;
 
    @Autowired
    private StepBuilderFactory stepBuilder;
    
    @Autowired
    private DataSource dataSource;

    @Autowired
    private ProveedorRepository proveedorRepository;

    private static String valorInicial;

    @Bean
    public Job readBBDDFile(@Qualifier("bbddtofileStep") Step bbddtofileStep) {
        return jobBuilder
                .get("readBBDDFile")
                .incrementer(new RunIdIncrementer())
                .start(bbddtofileStep)
                .build();
    }
 
    @Bean("bbddtofileStep")
    @JobScope
    public Step bbddtofileStep(@Qualifier("bbddtofile") Tasklet proveedorTasklet) {
        return stepBuilder.get("bbddtofileStep").tasklet(proveedorTasklet).build();
    }

    @Bean("bbddtofile")
    @StepScope
    public ProveedorTasklet proveedorTasklet(){
        ProveedorTasklet proveedorTasklet = new ProveedorTasklet();
        proveedorTasklet.setProveedorRepository(proveedorRepository);
        proveedorTasklet.setValorInicial(valorInicial);
        return proveedorTasklet;
    }

    public void setValorInicial(String arg) {
        valorInicial=arg;
    }
}