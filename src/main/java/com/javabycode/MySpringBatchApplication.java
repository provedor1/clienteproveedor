package com.javabycode;
 
import com.javabycode.batch.BatchJobConfig;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.swing.*;

@SpringBootApplication
@EnableScheduling
public class MySpringBatchApplication {

    @Autowired
    JobLauncher jobLauncher;
      
    @Autowired
    Job job;
      
    public static void main(String[] args) {
        BatchJobConfig batchJobConfig = new BatchJobConfig();
        batchJobConfig.setValorInicial(args[0]);
        SpringApplication.run(MySpringBatchApplication.class, args);
    }

    @Scheduled(cron = "0/10 * * * * ?")
    public void perform() throws Exception {
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job, params);
    }
}