package com.javabycode.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "PROVEEDOR")
public class Proveedor {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id_proveedor;
    String nombre;
    String fecha_de_alta;
    String id_cliente;

}